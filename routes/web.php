<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
// $router->get('/','TestController@test');
// $router->get('/key-generate', function(){
// 	$key = password_hash('admin', PASSWORD_DEFAULT);
// 	return $key;
// });

// Login
$router->post('/login','api\LoginController@loginAuth');
$router->group(['middleware' => 'token'], function () use ($router) {
	
	// Customer
	$router->group(['prefix' => 'agenda'], function () use ($router) {
		$router->post('/','api\AgendaController@get_data');
	});
	$router->group(['prefix' => 'list-bc'], function () use ($router) {
		$router->post('/','api\BcController@get_data');
	});
	$router->group(['prefix' => 'event'], function () use ($router) {
		$router->post('/','api\EventController@get_data');
	});
	$router->group(['prefix' => 'story'], function () use ($router) {
		$router->post('/','api\StoryController@get_data');
	});
	$router->group(['prefix' => 'asia'], function () use ($router) {
		$router->post('/','api\AsiaController@get_data');
	});
	$router->group(['prefix' => 'project'], function () use ($router) {
		$router->post('/','api\ProjectController@get_data');
	});
	$router->group(['prefix' => 'kontrak'], function () use ($router) {
		$router->post('/','api\KontrakController@get_data');
	});
	$router->group(['prefix' => 'piutang'], function () use ($router) {
		$router->post('/','api\PiutangController@get_data');
	});
	$router->group(['prefix' => 'rtb'], function () use ($router) {
		$router->post('/','api\RtbController@get_data');
	});

	// Channel
	$router->group(['prefix' => 'kebijakan'], function () use ($router) {
		$router->post('/','api\KebijakanController@get_data');
	});
	$router->group(['prefix' => 'format-docs'], function () use ($router) {
		$router->post('/','api\FormatDocsController@get_data');
	});
	$router->group(['prefix' => 'daftar-istilah'], function () use ($router) {
		$router->post('/','api\DaftarIstilahController@get_data');
	});
	$router->group(['prefix' => 'kipas-budaya'], function () use ($router) {
		$router->post('/','api\KipasBudayaController@get_data');
	});
	$router->group(['prefix' => 'list'], function () use ($router) {
		$router->post('/','api\SkemaListController@get_data');
	});
	$router->group(['prefix' => 'detail'], function () use ($router) {
		$router->post('/','api\SkemaDetailController@get_data');
	});

	// Product
	$router->group(['prefix' => 'product-solusi'], function () use ($router) {
		$router->post('/','api\ProductSolusiController@get_data');
	});
	$router->group(['prefix' => 'promo'], function () use ($router) {
		$router->post('/','api\PromoController@get_data');
	});
	$router->group(['prefix' => 'bussiness-intelegent'], function () use ($router) {
		$router->post('/','api\BussinessIntelegentController@get_data');
	});
	$router->group(['prefix' => 'profil-kompetitor'], function () use ($router) {
		$router->post('/','api\ProfileKompetitorController@get_data');
	});
	$router->group(['prefix' => 'pricing-kompetitor'], function () use ($router) {
		$router->post('/','api\PricingKompetitorController@get_data');
	});
	$router->group(['prefix' => 'skema-bisnis-kompetitor'], function () use ($router) {
		$router->post('/','api\SkemaBisnisKompetitorController@get_data');
	});

	// Home
	$router->group(['prefix' => 'kontak'], function () use ($router) {
		$router->post('/','api\KontakController@get_data');
	});
	$router->group(['prefix' => 'home-promo'], function () use ($router) {
		$router->post('/','api\PromoHomeController@get_data');
	});
	$router->group(['prefix' => 'piala'], function () use ($router) {
		$router->post('/','api\PialaController@get_data');
	});
	$router->group(['prefix' => 'performance'], function () use ($router) {
		$router->post('/','api\PerformanceController@get_data');
	});
	$router->group(['prefix' => 'dashboard-message'], function () use ($router) {
		$router->post('/','api\DashboardMessageController@get_data');
	});
});
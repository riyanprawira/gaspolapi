<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'cs_event';
    protected $primaryKey = 'event_id';
    protected $fillable = [
        'event_picture', 'event_judul', 'event_headline', 'event_desc', 'agenda_updated', 'event_date', 'event_updated'
    ];
    public $timestamps = false;
}
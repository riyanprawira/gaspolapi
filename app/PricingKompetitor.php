<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PricingKompetitor extends Model
{
    protected $table = 'pr_pricing';
    protected $primaryKey = 'pricing_id';
    protected $fillable = [
        'kompetitor_id', 'pricing_picture'
    ];
    protected $hidden = ['pricing_id'];
    public function kompetitor(){
        return $this->belongsTo(Kompetitor::class, 'kompetitor_id');
    }
}
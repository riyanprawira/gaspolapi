<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KipasBudaya extends Model
{
    protected $table = 'cn_kipas';
    protected $primaryKey = 'kipas_id';
    protected $fillable = [
        'kipas_picture', 'kipas_updated'
    ];
    protected $hidden = [
        'kipas_id', 'kipas_updated'
    ];
    public $timestamps = false;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'gp_karyawan';
    protected $primaryKey = 'karyawan_id';
    protected $fillable = [
        'karyawan_id', 'karyawan_nama', 'karyawan_witel', 'karyawan_subarea', 'karyawan_tr', 'karyawan_type', 'karyawan_status', 'karyawan_password'
    ];
    protected $hidden = [
        'karyawan_password'
    ];
    public $incrementing = false;
    public $timestamps = false;
    public function token(){
        return $this->hasMany(Token::class, 'karyawan_id', 'karyawan_id');
    }
    public function list_bc(){
        return $this->hasMany(ListBC::class, 'nik_am', 'karyawan_id');
    }
    public function performance(){
        return $this->hasMany(Performance::class, 'nik_am', 'karyawan_id');
    }
    public function activity(){
        return $this->hasMany(Activity::class, 'nik_am', 'karyawan_id');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'hm_activity';
    protected $primaryKey = 'act_id';
    protected $fillable = [
        'nik_am', 'act_date', 'act_updated'
    ];
    public $timestamps = false;
    public function karyawan(){
        return $this->belongsTo(Karyawan::class, 'nik_am');
    }
}
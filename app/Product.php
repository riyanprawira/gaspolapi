<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'pr_product';
    protected $primaryKey = 'product_id';
    protected $fillable = [
        'category_id', 'product_name', 'product_file', 'product_updated'
    ];
    protected $hidden = [
        'product_id', 'category_id', 'product_updated'
    ];
    public $timestamps = false;
    public function skema_detail(){
        return $this->belongsTo(Kategori::class, 'category_id');
    }
}
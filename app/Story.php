<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $table = 'cs_story';
    protected $primaryKey = 'story_id';
    protected $fillable = [
        'story_picture', 'story_judul', 'story_headline', 'story_desc', 'story_date', 'story_updated'
    ];
    public $timestamps = false;
}
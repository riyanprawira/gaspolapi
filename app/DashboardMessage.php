<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardMessage extends Model
{
    protected $table = 'hm_message';
    protected $primaryKey = 'message_id';
    protected $fillable = [
        'message_photo', 'message_name', 'message_isi', 'message_updated'
    ];
    protected $hidden = ['message_id'];
    public $timestamps = false;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkemaDetail extends Model
{
    protected $table = 'cn_skemadetail';
    protected $primaryKey = 'detail_id';
    protected $fillable = [
        'skema_id', 'detail_desc', 'detail_file'
    ];
    public $hidden = ['detail_id', 'skema_id'];
    public function skema_list(){
    	return $this->belongsTo(SkemaList::class, 'skema_id');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontak extends Model
{
    protected $table = 'hm_contact';
    protected $primaryKey = 'contact_id';
    protected $fillable = [
        'contact_name', 'contact_jabatan', 'contact_email', 'contact_nomor'
    ];
    protected $hidden = ['contact_id'];
}
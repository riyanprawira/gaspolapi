<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormatDocs extends Model
{
    protected $table = 'cn_docs';
    protected $primaryKey = 'docs_id';
    protected $fillable = [
        'docs_name', 'docs_file', 'docs_updated'
    ];
    public $timestamps = false;
}
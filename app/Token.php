<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
	protected $table = 'gp_token';
    protected $primaryKey = 'token_id';
    protected $fillable = ['karyawan_id', 'token_value', 'token_regid', 'token_regtoken', 'token_created'];
    public $timestamps = false;
    public function karyawan(){
        return $this->belongsTo(Karyawan::class, 'karyawan_id');
    }
}
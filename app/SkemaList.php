<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkemaList extends Model
{
    protected $table = 'cn_skemalist';
    protected $primaryKey = 'skema_id';
    protected $fillable = [
        'skema_name', 'skema_updated'
    ];
    public $timestamps = false;
    public $hidden = ['skema_updated'];
    public function skema_detail(){
        return $this->hasMany(SkemaDetail::class, 'skema_id', 'skema_id');
    }
}
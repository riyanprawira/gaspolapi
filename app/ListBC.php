<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListBC extends Model
{
	protected $table = 'cs_listbc';
	protected $primaryKey = 'listbc_id';
    protected $fillable = [
        'nipnas', 'standardname_group', 'witel_ho', 'div_tr_bc', 'nik_am', 'nama_am', 'kel_am_1', 'witel_segmen', 'div_tr', 'flagging_am_segmen', 'target_sustain_01', 'target_sustain_02', 'target_sustain_03', 'target_sustain_04', 'target_sustain_05', 'target_sustain_06', 'target_sustain_07', 'target_sustain_08', 'target_sustain_09', 'target_sustain_10', 'target_sustain_11', 'target_sustain_12', 'total_sustain'
    ];
    public $timestamps = false;
    public function karyawan(){
        return $this->belongsTo(Karyawan::class, 'nik_am');
    }
}
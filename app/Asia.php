<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asia extends Model
{
    protected $table = 'cs_asia';
    protected $primaryKey = 'asia_id';
    protected $fillable = [
        'kategori', 'project_type', 'project_name', 'value', 'day_of_const_start', 'day_of_const_end', 'l_town', 'regional', 'witel'
    ];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'cs_lop';
    protected $primaryKey = 'lop_id';
    protected $fillable = [
        'region', 'witel', 'nama_am', 'nik_am', 'nama_bc', 'nama_project', 'layanan_solution', 'portofolio_product', 'jangka_waktu_project_bulanan', 'nilai_project', 'nilai_otc', 'recurring_bulanan', 'nilai_bilcom', 'revenue_q2', 'revenue_tahun', 'estimasi_closed_win_bulan', 'status_progress', 'mitra_sinergi', 'level_of_confidence', 'potensi_new_gtma_model', 'keterangan_progres', 'principal_partner', 'bidding_time_bulan', 'gross_profit_margin', 'revenue_jan', 'revenue_feb', 'revenue_mar', 'revenue_apr', 'revenue_mei', 'revenue_jun', 'revenue_jul', 'revenue_aug', 'revenue_sep', 'revenue_oct', 'revenue_nov', 'revenue_des', 'keterangan_progres2'
    ];
}
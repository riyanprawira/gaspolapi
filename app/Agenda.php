<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $table = 'cs_agenda';
    protected $primaryKey = 'agenda_id';
    protected $fillable = [
        'agenda_judul', 'agenda_lokasi', 'agenda_sales', 'agenda_tanggal', 'agenda_updated'
    ];
    public $timestamps = false;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kebijakan extends Model
{
    protected $table = 'cn_kebijakan';
    protected $primaryKey = 'kebijakan_id';
    protected $fillable = [
        'kebijakan_name', 'kebijakan_file', 'kebijakan_updated'
    ];
    public $timestamps = false;
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kontrak extends Model
{
    protected $table = 'cs_kontrak';
    protected $primaryKey = 'kontrak_id';
    protected $fillable = [
        'no_order', 'no_account', 'nama_bc', 'nama_cp', 'notel_cp', 'reg', 'witel', 'segmen', 'akhir_kontrak', 'sisa_umur', 'level_eskalasi', 'no_bprole_am', 'nik_am', 'nama_am', 'nik_am_pengelola', 'status', 'tanggal_akhir_kontrak', 'umur', 'ba_lock', 'process_type', 'typedesc', 'abo', 'paytermdesc', 'productname', 'speedconfig', 'currency', 'agree_name'
    ];
    public $timestamps = false;
}
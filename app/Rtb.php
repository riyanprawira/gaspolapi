<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rtb extends Model
{
    protected $table = 'cs_bill';
    protected $primaryKey = 'bill_id';
    protected $fillable = [
        'nipnas', 'nama_bc', 'order_id', 'regional', 'witel', 'li_milestone', 'li_product_name', 'li_createdby_name', 'nik_am', 'nama_am'
    ];
    public $timestamps = false;
}
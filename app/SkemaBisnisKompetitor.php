<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkemaBisnisKompetitor extends Model
{
    protected $table = 'pr_skema';
    protected $primaryKey = 'skema_id';
    protected $fillable = [
        'kompetitor_id', 'skema_desc'
    ];
    protected $hidden = ['skema_id'];
    public function kompetitor(){
        return $this->belongsTo(Kompetitor::class, 'kompetitor_id');
    }
}
<?php

namespace App\Http\Middleware;

use Closure;
use App\Token as Token;
class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->header('Api-Token')){
            return response()->json([
                'message' => 'Not a valid API request, Token is Required',
            ], 404);
        } else if($request->header('Api-Token')){
            $token = $request->header('Api-Token');
            $check = Token::where('token_value', $token)->first();
            if($check){
                return $next($request);
            } else {
                return response()->json([
                    'token' => 'Token Not Valid'
                ], 500);
            }
        }
    }
}

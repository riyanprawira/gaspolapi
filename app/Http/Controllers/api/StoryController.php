<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Story as Story;

class StoryController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Story::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Story::where('story_judul', 'like', '%'.$search.'%')
                    ->orWhere('story_headline', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('story_updated', 'DESC')
                    ->get();
            $total_filter = Story::where('story_judul', 'like', '%'.$search.'%')
                            ->orWhere('story_headline', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Story::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('story_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['story_picture'])){
                    $dt['story_picture_link'] = url('/').'/photos/story/'.$dt['story_picture'];
                } else {
                    $dt['story_picture_link'] = 'Gambar tidak ditemukan!';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

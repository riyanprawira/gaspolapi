<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Piala as Piala;

class PialaController extends Controller
{
    public function get_data(Request $request)
    {
        // $piala = Piala::where($search)->paginate(10);
        // if ($piala) {
        //     if (count($piala) > 0) {
        //         foreach ($piala as $value) {
        //             if (!empty($value['piala_top']) && !empty($value['piala_lose'])) {
        //                 $value['piala_top'] = url('/').'/photos/piala/'.$value['piala_top'];
        //                 $value['piala_lose'] = url('/').'/photos/piala/'.$value['piala_lose'];
        //             } else {
        //                 $value['piala_top'] = 'Foto tidak ditemukan!';
        //                 $value['piala_lose'] = 'Foto tidak ditemukan!';
        //             }
        //         }
        //     }
        //     return response()->json($piala, 200);
        // } else {
        //     $response['message'] = 'Data tidak ditemukan!';
        //     return response()->json($response, 500);
        // }

        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Piala::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Piala::offset($offset)->limit($dataPerPage)
                    ->orderBy('piala_updated', 'DESC')
                    ->get();
            $total_filter = Piala::get()->count();
        } else {
            $data = Piala::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('piala_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['piala_top']) && !empty($dt['piala_lose'])){
                    $dt['piala_top'] = url('/').'/photos/piala/'.$dt['piala_top'];
                    $dt['piala_lose'] = url('/').'/photos/piala/'.$dt['piala_lose'];
                } else {
                    $dt['piala_top'] = 'Not found picture/image!';
                    $dt['piala_lose'] = 'Not found picture/image!';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

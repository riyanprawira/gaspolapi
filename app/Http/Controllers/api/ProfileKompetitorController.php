<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProfileKompetitor as Profile;

class ProfileKompetitorController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Profile::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Profile::where('profil_desc', 'like', '%'.$search.'%')
                    ->orWhere('kompetitor_id', $search)
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('profil_id', 'ASC')
                    ->get();
            $total_filter = Profile::where('profil_desc', 'like', '%'.$search.'%')
                            ->orWhere('kompetitor_id', $search)
                            ->get()
                            ->count();
        } else {
            $data = Profile::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('profil_id', 'ASC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'No Data Found!';
            return response()->json($response, 500);
        }
    }
}

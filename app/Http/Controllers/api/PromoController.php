<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PromoHome as Promo;
use App\Kategori as Kategori;

class PromoController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Promo::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Promo::where('category_id', '=', $search)
                    ->orWhere('promo_title', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('promo_updated', 'DESC')
                    ->get();
            $total_filter = Promo::where('category_id', '=', $search)
                            ->orWhere('promo_title', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Promo::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('promo_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['promo_picture'])){
                    $dt['promo_picture_link'] = url('/').'/photos/promo/'.$dt['promo_picture'];
                } else {
                    $dt['promo_picture_link'] = 'no picture';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FormatDocs as FormatDocs;

class FormatDocsController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = FormatDocs::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = FormatDocs::where('docs_name', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('docs_updated', 'DESC')
                    ->get();
            $total_filter = FormatDocs::where('docs_name', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = FormatDocs::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('docs_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['docs_file'])){
                    $dt['docs_file_link'] = url('/').'/photos/docs/'.$dt['docs_file'];
                } else {
                    $dt['docs_file_link'] = 'no picture';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kebijakan as Kebijakan;

class KebijakanController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Kebijakan::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Kebijakan::where('kebijakan_name', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('kebijakan_updated', 'DESC')
                    ->get();
            $total_filter = Kebijakan::where('kebijakan_name', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Kebijakan::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('kebijakan_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['kebijakan_file'])){
                    $dt['kebijakan_file_link'] = url('/').'/photos/kebijakan/'.$dt['kebijakan_file'];
                } else {
                    $dt['kebijakan_file_link'] = 'no picture';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\KipasBudaya as KipasBudaya;

class KipasBudayaController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = KipasBudaya::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = KipasBudaya::offset($offset)->limit($dataPerPage)
                    ->orderBy('kipas_updated', 'DESC')
                    ->get();
            $total_filter = KipasBudaya::get()->count();
        } else {
            $data = KipasBudaya::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('kipas_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['kipas_picture'])){
                    $dt['kipas_picture_link'] = url('/').'/photos/kipas/'.$dt['kipas_picture'];
                } else {
                    $dt['kipas_picture_link'] = 'no picture';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event as Event;

class EventController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Event::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Event::where('event_judul', 'like', '%'.$search.'%')
                    ->orWhere('event_headline', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('event_updated', 'DESC')
                    ->get();
            $total_filter = Event::where('event_judul', 'like', '%'.$search.'%')
                            ->orWhere('event_headline', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Event::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('event_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['event_picture'])){
                    $dt['event_picture_link'] = url('/').'/photos/event/'.$dt['event_picture'];
                } else {
                    $dt['event_picture_link'] = 'no picture';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

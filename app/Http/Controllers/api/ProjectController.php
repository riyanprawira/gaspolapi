<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Project as Project;

class ProjectController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = DB::table('projectku')->count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = DB::table('projectku')
                    ->where('nama_bc', 'like', '%'.$search.'%')
                    ->orWhere('witel', 'like', '%'.$search.'%')
                    ->orWhere('layanan_solution', 'like', '%'.$search.'%')
                    ->orWhere('mitra_sinergi', 'like', '%'.$search.'%')
                    ->orWhere('nik_am', 'like', '%'.$search.'%')
                    ->orWhere('nama_am', 'like', '%'.$search.'%')
                    ->orWhere('region', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('nama_bc', 'DESC')
                    ->get();
            $total_filter = DB::table('projectku')
                            ->where('nama_bc', 'like', '%'.$search.'%')
                            ->orWhere('witel', 'like', '%'.$search.'%')
                            ->orWhere('layanan_solution', 'like', '%'.$search.'%')
                            ->orWhere('mitra_sinergi', 'like', '%'.$search.'%')
                            ->orWhere('nik_am', 'like', '%'.$search.'%')
                            ->orWhere('nama_am', 'like', '%'.$search.'%')
                            ->orWhere('region', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = DB::table('projectku')->offset($offset)
                ->limit($dataPerPage)
                ->orderBy('nama_bc', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['file'] = url('/').'/photos/template/projectku.xls';
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

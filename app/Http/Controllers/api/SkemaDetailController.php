<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SkemaDetail as SkemaDetail;

class SkemaDetailController extends Controller
{
    public function get_data(Request $request)
    {
        // $param = $request->except(['page','search']);
        // $search = [];
        // if(count($param) > 0){
        //     foreach($param as $key => $value){
        //         if(!empty($value)){
        //             array_push($search, [$key, 'like' , '%'.$value.'%']);
        //         }
        //     }
        // }
        // $data = SkemaDetail::where($search)->paginate(10);
        // if ($data) {
        //     if (count($data) > 0) {
        //         foreach ($data as $value) {
        //             if (!empty($value['detail_file'])) {
        //                 $value['detail_file'] = url('/').'/photos/skema/'.$value['detail_file'];
        //             } else {
        //                 $value['detail_file'] = 'File tidak ditemukan!';
        //             }
        //         }
        //     }
        //     return response()->json($data, 200);
        // } else {
        //     $response['message'] = 'Data tidak ditemukan!';
        //     return response()->json($response, 500);
        // }

        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = SkemaDetail::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = SkemaDetail::where('skema_id', $search)
                    ->offset($offset)->limit($dataPerPage)
                    ->orderBy('detail_id', 'ASC')
                    ->get();
            $total_filter = SkemaDetail::where('skema_id', $search)->get()->count();
        } else {
            $data = SkemaDetail::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('detail_id', 'ASC')
                ->get();
        }

        if (!empty($data)) {
            foreach ($data as $value) {
                if (!empty($value['detail_file'])) {
                    $value['detail_file_link'] = url('/').'/photos/skema/'.$value['detail_file'];
                } else {
                    $value['detail_file_link'] = 'File tidak ditemukan!';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

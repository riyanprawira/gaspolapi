<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Agenda as Agenda;

class AgendaController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Agenda::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Agenda::where('agenda_judul', 'like', '%'.$search.'%')
                    ->orWhere('agenda_lokasi', 'like', '%'.$search.'%')
                    ->orWhere('agenda_sales', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('agenda_updated', 'DESC')
                    ->get();
            $total_filter = Agenda::where('agenda_judul', 'like', '%'.$search.'%')
                            ->orWhere('agenda_lokasi', 'like', '%'.$search.'%')
                            ->orWhere('agenda_sales', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Agenda::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('agenda_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'No Data Found!';
            return response()->json($response, 500);
        }
    }
}

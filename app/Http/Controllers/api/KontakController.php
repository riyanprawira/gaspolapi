<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Kontak as Kontak;

class KontakController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Kontak::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Kontak::where('contact_name', 'like', '%'.$search.'%')
                    ->orWhere('contact_jabatan', 'like', '%'.$search.'%')
                    ->orWhere('contact_email', 'like', '%'.$search.'%')
                    ->orWhere('contact_nomor', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('contact_name', 'ASC')
                    ->get();
            $total_filter = Kontak::where('contact_name', 'like', '%'.$search.'%')
                            ->orWhere('contact_jabatan', 'like', '%'.$search.'%')
                            ->orWhere('contact_email', 'like', '%'.$search.'%')
                            ->orWhere('contact_nomor', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Kontak::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('contact_name', 'ASC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

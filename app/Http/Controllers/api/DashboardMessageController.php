<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DashboardMessage as DM;
use App\Performance as Perform;
use App\Activity as Act;
use App\Karyawan as Karyawan;

class DashboardMessageController extends Controller
{
    public function get_data(Request $request)
    {
        $karyawan_id = $request->input('karyawan_id');
        $response['data'] = [];
        if ($karyawan_id) {

        // Insert Or Update Activity
        $date = date('Y-m-d H:i:s');
        Act::UpdateOrCreate(
            ['nik_am' => $karyawan_id], 
            ['act_date' => $date]
        );

        // Dashboard
        $result['dashboard'] = [];
        $dashboard = [];
        $perform = Perform::where('nik_am', '=', $karyawan_id)->first();
        $karyawan = Karyawan::where('karyawan_type', '=', 2)->get()->count();

        if ($perform['acm_scalling'] >= 70) {
            $acm = 'MANTUL';
        } else {
            $acm = 'CUMAN SEGINI???';
        }

        if ($perform['aytd_scalling'] >= 70) {
            $aytd = 'MANTUL';
        } else {
            $aytd = 'CUMAN SEGINI???';
        }

        $dashboard = [
            'am_rank' => $perform['am_rank'].'/'.$karyawan,
            'cm_score' => str_replace('%', '', $perform['acm_scalling']),
            'cm_word' => $acm,
            'ytd_score' => str_replace('%', '', $perform['aytd_scalling']),
            'ytd_word' => $aytd
        ];
        array_push($result['dashboard'], $dashboard);

        // Message
        $result['message'] = [];
        $message = [];
        $msg = DM::all();
        if (count($msg) > 0) {
            foreach ($msg as $value) {
                if (!empty($value['message_photo'])) {
                    $value['message_photo'] = url('/').'/photos/message/'.$value['message_photo'];
                } else {
                    $value['message_photo'] = 'Foto tidak ditemukan!';
                }
            }
        }
        $message = [
            'message_photo' => $value['message_photo'],
            'message_isi' => $value['message_isi'],
            'message_name' => $value['message_name'],
            'message_date' => $value['message_updated'],
        ];
        array_push($result['message'], $message);
        array_push($response['data'], $result);
        $response['success'] = 1;
        return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Input first karyawan id!';
            return response()->json($response, 200);
        }
    }
}

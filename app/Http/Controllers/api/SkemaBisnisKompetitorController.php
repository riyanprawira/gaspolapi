<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SkemaBisnisKompetitor as Skema;

class SkemaBisnisKompetitorController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Skema::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Skema::where('kompetitor_id', '=', $search)
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('skema_id', 'ASC')
                    ->get();
            $total_filter = Skema::where('kompetitor_id', '=', $search)
                            ->get()
                            ->count();
        } else {
            $data = Skema::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('skema_id', 'ASC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'No Data Found!';
            return response()->json($response, 500);
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KontrakController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = DB::table('kontrak')->count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = DB::table('kontrak')->where('nama_bc', 'like', '%'.$search.'%')
                    ->orWhere('reg', 'like', '%'.$search.'%')
                    ->orWhere('witel', 'like', '%'.$search.'%')
                    ->orWhere('sisa_umur', 'like', '%'.$search.'%')
                    ->orWhere('nama_am', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('akhir_kontrak', 'DESC')
                    ->get();
            $total_filter = DB::table('kontrak')->where('nama_bc', 'like', '%'.$search.'%')
                            ->orWhere('reg', 'like', '%'.$search.'%')
                            ->orWhere('witel', 'like', '%'.$search.'%')
                            ->orWhere('sisa_umur', 'like', '%'.$search.'%')
                            ->orWhere('nama_am', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = DB::table('kontrak')->offset($offset)
                ->limit($dataPerPage)
                ->orderBy('akhir_kontrak', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['file'] = url('/').'/photos/template/kontrak.xls';
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

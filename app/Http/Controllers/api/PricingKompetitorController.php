<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\PricingKompetitor as Pricing;

class PricingKompetitorController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Pricing::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Pricing::where('kompetitor_id', $search)
                    ->offset($offset)->limit($dataPerPage)
                    ->orderBy('pricing_id', 'ASC')
                    ->get();
            $total_filter = Pricing::where('kompetitor_id', $search)
                            ->get()
                            ->count();
        } else {
            $data = Pricing::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('pricing_id', 'ASC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['pricing_picture'])){
                    $dt['pricing_picture_link'] = url('/').'/photos/pricing/'.$dt['pricing_picture'];
                } else {
                    $dt['pricing_picture_link'] = 'no picture';
                }
            }
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Performance as Performance;

class PerformanceController extends Controller
{
    public function get_data(Request $request)
    {
        $karyawan_id = $request->input('karyawan_id');
        if ($karyawan_id) {
            $cek = Performance::where('nik_am', $karyawan_id)->first();
            if ($cek == true) {
                $cek['success'] = 1;
                return response()->json($cek, 200);
            } else {
                $response['success'] = 0;
                $response['message'] = 'Data Karyawan ID Not Found!';
                return response()->json($response, 500);
            }
        } else {
            $response['success'] = 0;
            $response['message'] = 'Please Input Karyawan ID';
            return response()->json($response, 500);
        }
    }
}

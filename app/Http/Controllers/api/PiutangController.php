<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Piutang as Piutang;

class PiutangController extends Controller
{
    public function get_data(Request $request)
    {
        // try{
        //     $total = Piutang::where($search)->get()->count();
        //     $response = [
        //         "recordsTotal" => $total,
        //         "recordsFiltered" => $total
        //     ];
        //     $bc = Piutang::where($search)->offset($request->query('page') ?? 0)
        //             ->limit($request->query('limit') ?? 10)
        //             ->orderBy($request->has('order') ? $request->get('order') : 'piutang_id', $request->has('sort') ? $request->get('sort') : 'asc')
        //             ->get();
        //     $response['data'] = $bc;
        //     $response['file'] = url('/').'/photos/template/piutang.xls';
        //     return response()->json($response, 200);
        // } catch(\Exception $e){
        //     return response()->json($e->getMessage(), $e);
        // }

        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Piutang::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Piutang::where('business_partner', 'like', '%'.$search.'%')
                    ->orWhere('nama_bc', 'like', '%'.$search.'%')
                    ->orWhere('legal_action', 'like', '%'.$search.'%')
                    ->orWhere('kolektabilitas_pu', 'like', '%'.$search.'%')
                    ->orWhere('nama_am', 'like', '%'.$search.'%')
                    ->orWhere('witel', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('invoice_cm', 'DESC')
                    ->get();
            $total_filter = Piutang::where('business_partner', 'like', '%'.$search.'%')
                            ->orWhere('nama_bc', 'like', '%'.$search.'%')
                            ->orWhere('legal_action', 'like', '%'.$search.'%')
                            ->orWhere('kolektabilitas_pu', 'like', '%'.$search.'%')
                            ->orWhere('nama_am', 'like', '%'.$search.'%')
                            ->orWhere('witel', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Piutang::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('invoice_cm', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['file'] = url('/').'/photos/template/piutang.xls';
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

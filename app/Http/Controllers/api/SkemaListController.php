<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SkemaList as SkemaList;

class SkemaListController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = SkemaList::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = SkemaList::where('skema_name', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('skema_id', 'ASC')
                    ->get();
            $total_filter = SkemaList::where('skema_name', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = SkemaList::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('skema_id', 'ASC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'No Data Found!';
            return response()->json($response, 500);
        }
    }
}

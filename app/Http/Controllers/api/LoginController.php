<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Karyawan as Karyawan;
use App\Token as Token;

class LoginController extends Controller
{
    public function loginAuth(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'karyawan_id' => 'required|max:100',
            'karyawan_password' => 'required|max:100',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 500);
        }

        // $response['data'] = [];
        $data = Karyawan::where('karyawan_id', $input['karyawan_id'])->first();
        // verify karyawan
        if ($data) {
            // verify password
            if (Hash::check($input['karyawan_password'], $data->karyawan_password)) {
                // verify status
                if ($data->karyawan_status == 2) {
                    $response['message'] = 'Login Gagal, Akun anda dinonaktifkan';
                    return response()->json($response, 500);
                } else {
                    // token
                    $response['success'] = 1;
                    $token = $data->karyawan_id.date('YdmisH').rand(000000,999999);
                    //response
                    $dataRes = [
                        'message' => 'Selamat datang '.$data->karyawan_nama.'!',
                        'karyawan_id' => $data->karyawan_id,
                        'karyawan_nama' => $data->karyawan_nama,
                        'karyawan_type' => $data->karyawan_type,
                        'karyawan_status' => $data->karyawan_status,
                        'token' => $token
                    ];
                    // token update jika sudah ada dan token create jika belum ada
                    Token::UpdateOrCreate(
                        ['karyawan_id' => $data->karyawan_id], 
                        ['token_value' => $dataRes['token']]
                    );
                    // array_push($response['data'], $dataRes);
                    return response()->json($dataRes, 200);
                }
            } else {
                $response['message'] = 'Username atau Password Salah!';
                $response['success'] = 0;
                return response()->json($response, 500);
            }
        } else {
            $response['message'] = 'Username tidak terdaftar!';
            $response['success'] = 0;
            return response()->json($response, 404);
        }
    }
}

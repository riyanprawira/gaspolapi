<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BcController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = DB::table('view_listbc')->count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = DB::table('view_listbc')
                    ->where('standardname_group', 'like', '%'.$search.'%')
                    ->orWhere('witel_ho', 'like', '%'.$search.'%')
                    ->orWhere('div_tr_bc', 'like', '%'.$search.'%')
                    ->orWhere('nik_am', 'like', '%'.$search.'%')
                    ->orWhere('nama_am', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('listbc_id', 'DESC')
                    ->get();
            $total_filter = DB::table('view_listbc')
                            ->where('standardname_group', 'like', '%'.$search.'%')
                            ->orWhere('witel_ho', 'like', '%'.$search.'%')
                            ->orWhere('div_tr_bc', 'like', '%'.$search.'%')
                            ->orWhere('nik_am', 'like', '%'.$search.'%')
                            ->orWhere('nama_am', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = DB::table('view_listbc')->offset($offset)
                ->limit($dataPerPage)
                ->orderBy('listbc_id', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['file'] = url('/').'/photos/template/list_bc.xls';
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'No Data Found!';
            return response()->json($response, 500);
        }
    }
}

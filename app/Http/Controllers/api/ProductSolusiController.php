<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Product as Product;
use App\Kategori as Kategori;

class ProductSolusiController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Product::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');
        $kategori = $request->input('category_id');

        if ($search) {
            $data = Product::where('category_id', $search)
                    ->orWhere('product_name', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('product_updated', 'DESC')
                    ->get();
            $total_filter = Product::where('category_id', $search)
                            ->orWhere('product_name', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Product::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('product_updated', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            foreach($data as $dt){
                if(!empty($dt['product_file'])){
                    $dt['product_file_link'] = url('/').'/photos/produk/'.$dt['product_file'];
                } else {
                    $dt['product_file_link'] = 'no picture';
                }
            }

            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $kategori = Kategori::where('category_id', $kategori)->first();
            $response['deskripsi'] = $kategori['category_desc'];
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

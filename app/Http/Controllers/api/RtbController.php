<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Rtb as Rtb;

class RtbController extends Controller
{
    public function get_data(Request $request)
    {
        // $param = $request->except(['page','search']);
        // $search = [];
        // if(count($param) > 0){
        //     foreach($param as $key => $value){
        //         if(!empty($value)){
        //             array_push($search, [$key, 'like' , '%'.$value.'%']);
        //         }
        //     }
        // }
        // try{
        //     $total = DB::table('view_bill')->where($search)->get()->count();
        //     $response = [
        //         "recordsTotal" => $total,
        //         "recordsFiltered" => $total
        //     ];
        //     $bc = DB::table('view_bill')->where($search)->offset($request->query('page') ?? 0)
        //             ->limit($request->query('limit') ?? 10)
        //             ->orderBy($request->has('order') ? $request->get('order') : 'order_id', $request->has('sort') ? $request->get('sort') : 'asc')
        //             ->get();
        //     $response['data'] = $bc;
        //     $response['file'] = url('/').'/photos/template/readytobill.xls';
        //     return response()->json($response, 200);
        // } catch(\Exception $e){
        //     return response()->json($e->getMessage(), $e);
        // }

        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = DB::table('view_bill')->count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = DB::table('view_bill')
                    ->where('nama_bc', 'like', '%'.$search.'%')
                    ->orWhere('order_id', 'like', '%'.$search.'%')
                    ->orWhere('regional', 'like', '%'.$search.'%')
                    ->orWhere('witel', 'like', '%'.$search.'%')
                    ->orWhere('li_milestone', 'like', '%'.$search.'%')
                    ->orWhere('li_product_name', 'like', '%'.$search.'%')
                    ->orWhere('li_createdby_name', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('nama_bc', 'DESC')
                    ->get();
            $total_filter = DB::table('view_bill')
                            ->where('nama_bc', 'like', '%'.$search.'%')
                            ->orWhere('order_id', 'like', '%'.$search.'%')
                            ->orWhere('regional', 'like', '%'.$search.'%')
                            ->orWhere('witel', 'like', '%'.$search.'%')
                            ->orWhere('li_milestone', 'like', '%'.$search.'%')
                            ->orWhere('li_product_name', 'like', '%'.$search.'%')
                            ->orWhere('li_createdby_name', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = DB::table('view_bill')->offset($offset)
                ->limit($dataPerPage)
                ->orderBy('nama_bc', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['file'] = url('/').'/photos/template/readytobill.xls';
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

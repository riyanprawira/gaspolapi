<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Asia as Asia;

class AsiaController extends Controller
{
    public function get_data(Request $request)
    {
        $page = $request->input('page');
        $dataPerPage = 10;

        if(!$page){
            $page = 1;
        }
        if($page){
            $noPage = $page;
        }else{
            $noPage = 1;
        }

        $total_data = Asia::count();
        $total_filter = $total_data;
        $offset = ($noPage - 1) * $dataPerPage;
        $search = $request->input('search');

        if ($search) {
            $data = Asia::where('witel', 'like', '%'.$search.'%')
                    ->orWhere('project_name', 'like', '%'.$search.'%')
                    ->orWhere('project_type', 'like', '%'.$search.'%')
                    ->orWhere('l_town', 'like', '%'.$search.'%')
                    ->orWhere('regional', 'like', '%'.$search.'%')
                    ->offset($offset)
                    ->limit($dataPerPage)
                    ->orderBy('asia_id', 'DESC')
                    ->get();
            $total_filter = Asia::where('witel', 'like', '%'.$search.'%')
                            ->orWhere('project_name', 'like', '%'.$search.'%')
                            ->orWhere('project_type', 'like', '%'.$search.'%')
                            ->orWhere('l_town', 'like', '%'.$search.'%')
                            ->orWhere('regional', 'like', '%'.$search.'%')
                            ->get()
                            ->count();
        } else {
            $data = Asia::offset($offset)
                ->limit($dataPerPage)
                ->orderBy('asia_id', 'DESC')
                ->get();
        }

        if (!empty($data)) {
            $response = [
                "data" => $data,
                "recordsTotal" => $total_data,
                "recordsFiltered" => $total_filter
            ];
            $response['file'] = url('/').'/photos/template/bci_asia.xls';
            $response['success'] = 1;
            return response()->json($response, 200);
        } else {
            $response['success'] = 0;
            $response['message'] = 'Data Not Found';
            return response()->json($response, 200);
        }
    }
}

<?php

namespace App\Http\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Karyawan as Karyawan;
use Illuminate\Http\Request;

class TestController extends BaseController
{
    public function test()
    {
    	$karyawan = Karyawan::all();
    	return view('read', compact('karyawan'));
    }
}

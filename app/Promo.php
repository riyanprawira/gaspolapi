<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table = 'pr_promo';
    protected $primaryKey = 'promo_id';
    protected $fillable = [
        'category_id', 'promo_picture', 'promo_title', 'promo_expired', 'promo_desc', 'promo_updated'
    ];
    protected $hidden = [
        'promo_id', 'promo_updated'
    ];
    public $timestamps = false;
    public function skema_detail(){
        return $this->belongsTo(Kategori::class, 'category_id');
    }
}
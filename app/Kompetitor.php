<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kompetitor extends Model
{
    protected $table = 'pr_kompetitor';
    protected $primaryKey = 'kompetitor_id';
    protected $fillable = ['kompetitor_name'];
    public function profile_kompetitor(){
        return $this->hasMany(ProfileKompetitor::class, 'kompetitor_id', 'kompetitor_id');
    }
}
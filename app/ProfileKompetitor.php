<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileKompetitor extends Model
{
    protected $table = 'pr_profil';
    protected $primaryKey = 'profil_id';
    protected $fillable = [
        'kompetitor_id', 'profil_desc'
    ];
    protected $hidden = ['profil_id'];
    public function kompetitor(){
        return $this->belongsTo(Kompetitor::class, 'kompetitor_id');
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piutang extends Model
{
    protected $table = 'cs_piutang';
    protected $primaryKey = 'piutang_id';
    protected $fillable = [
        'business_partner', 'nama_bc', 'nik_am', 'nama_am', 'reg', 'witel', 'kolektabilitas_pu', 'legal_action', 'invoice_cm', 'sudah_invoice'
    ];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DaftarIstilah extends Model
{
    protected $table = 'cn_istilah';
    protected $primaryKey = 'istilah_id';
    protected $fillable = ['istilah_desc'];
    protected $hidden = ['istilah_id'];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    protected $table = 'hm_perform';
    protected $primaryKey = 'perform_id';
    protected $fillable = [
        'tcm_sustain', 'tytd_sustain', 'tcm_scalling', 'tytd_scalling', 'rcm_sustain', 'rytd_sustain', 'rcm_scalling', 'rytd_scalling', 'acm_sustain', 'aytd_sustain', 'acm_scalling', 'aytd_scalling', 'cm_score', 'ytd_score', 'am_rank', 'nik_am', 'nama_am', 'level', 'witel', 'devisi', 'flagging'
    ];
    public function karyawan(){
        return $this->belongsTo(Karyawan::class, 'nik_am');
    }
}
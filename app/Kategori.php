<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'pr_category';
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'category_icon', 'category_name', 'category_desc', 'category_updated'
    ];
    public $timestamps = false;
    public function product(){
        return $this->hasMany(Product::class, 'category_id', 'category_id');
    }
}
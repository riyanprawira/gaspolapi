<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Piala extends Model
{
    protected $table = 'hm_piala';
    protected $primaryKey = 'piala_id';
    protected $fillable = [
        'piala_top', 'piala_lose', 'piala_updated'
    ];
    protected $hidden = [
        'piala_id', 'piala_updated'
    ];
    public $timestamps = false;
}